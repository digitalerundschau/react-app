const employees = [
    {
        id: 1,
        firstname: 'James',
        lastname: 'Don',
        email: 'jamesdon@example.com'
    },
    {
        id: 2,
        firstname: 'Melanie',
        lastname: 'Rain',
        email: 'melrain@example.com'
    },
];

function List() {
    return (
        <table>
            <thead>
            <tr>
                <th>ID</th>
                <th>Vorname</th>
                <th>Nachname</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            { employees.map((employee) => (
                <tr key={employee.id}>
                    <td>{employee.id}</td>
                    <td>{employee.firstname}</td>
                    <td>{employee.lastname}</td>
                    <td>{employee.email}</td>
                </tr>
            )) }
            </tbody>
        </table>
    );
}

export default List;
